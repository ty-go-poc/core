module gitlab.com/ty-go-poc/core

go 1.13

require (
	cloud.google.com/go v0.38.0
	github.com/golang/protobuf v1.3.2
	github.com/jmoiron/sqlx v1.2.0
	github.com/lib/pq v1.2.0
	github.com/micro/go-micro v1.18.0
	github.com/rs/zerolog v1.17.2
	github.com/sirupsen/logrus v1.4.2
	gopkg.in/yaml.v2 v2.2.7
)
