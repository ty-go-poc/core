package core

import (
	"fmt"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq" // nolint:golint
	"github.com/sirupsen/logrus"
	"net/http"
)

var (
	dbStore     *DBStore = nil
	testDbStore *DBStore = nil
)

// StoreImpl implementation for store interface
type DBStore struct {
	usr    string
	dbName string
	pass   string
	host   string
	port   int
	Db     *sqlx.DB
	log    *logrus.Logger
	IsTest *bool
	Mocked *bool
}

func (s *DBStore) connect() bool {
	connStr := fmt.Sprintf(
		"sslmode=disable user='%s' password='%s' host='%s' port='%d' dbname='%s'",
		s.usr, s.pass, s.host, s.port, s.dbName)
	db, err := sqlx.Open("postgres", connStr)
	if err != nil {
		s.log.Fatal(err)
		return false
	}
	s.Db = db
	return true
}

// init DBStore and start a connection
func (s *DBStore) init(user string, dbname string, password string, host string, port int, log *logrus.Logger) {
	s.usr = user
	s.dbName = dbname
	s.pass = password
	s.host = host
	s.port = port
	s.log = log
	s.connect()
}

// CloseConnection to store
func (s *DBStore) closeConnection() bool {
	if s.Db == nil {
		return false
	}
	if err := s.Db.Close(); err != nil {
		return false
	}
	return true
}

// AssertConnectionOrReconnect assert connection to database or try to reconnect
func (s *DBStore) assertConnectionOrReconnect() bool {
	db := s.Db
	if s.Db == nil || db.Ping() != nil {
		s.log.Info("No connection to DB")
		_ = s.closeConnection()
		if ok := s.connect(); !ok {
			s.log.Error("Could not reconnect to DB")
			return false
		}
	}
	if db.Ping() != nil {
		s.log.Error("No connection to Database")
		return false
	}
	return true
}

type Options struct {
	isTest     bool
	mockedTest bool
	config     *DbConfig
	logger     *logrus.Logger
}

// Read option to get or set up correct database
// options:
// isTest -> bool: set/get Test database
// mockedTest -> bool: When setting test database, configure it as a mocked interface
// config -> *DbConfig: When setting up database connection, use this config
// logger -> *logrus.Logger: When setting up database connection, use this logger
func parseGetOption(options map[string]interface{}) Options {
	opt := Options{
		isTest:     false,
		mockedTest: true,
		config:     nil,
		logger:     nil,
	}

	// Parse options
	for key, value := range options {
		switch key {
		case "test":
			tmpT, ok := value.(bool)
			if ok {
				opt.isTest = tmpT
			}
		case "mockTest":
			tmpM, ok := value.(bool)
			if ok {
				opt.mockedTest = tmpM
			}
		case "config":
			conf, ok := value.(DbConfig)
			if ok {
				opt.config = &conf
			}
		case "logger":
			lg, ok := value.(*logrus.Logger)
			if ok {
				opt.logger = lg
			}
		default:
			// ignore
		}
	}
	return opt
}

func (s *DBStore) getOrInit(opt Options) (*sqlx.DB, error) {
	if s != nil {
		if ok := s.assertConnectionOrReconnect(); !ok {
			return nil, &TYPoc{
				Code:    http.StatusServiceUnavailable,
				From:    "get_db",
				Element: "connection",
				Message: "Could not connect to Database",
			}
		}
		return s.Db, nil
	}
	if opt.config == nil {
		return nil, &TYPoc{
			Code:    http.StatusNotAcceptable,
			From:    "get_db",
			Element: "config",
			Message: "Config object is required when initializing a new connection",
		}
	}

	s.init(
		opt.config.User, opt.config.Database,
		opt.config.Password, opt.config.Host, opt.config.Port,
		opt.logger,
	)

	if s.log == nil {
		s.log = logrus.New()
	}

	return s.Db, nil
}

// Db get database connection
// Config options is required when initializing database !!!
func Db(options ...map[string]interface{}) (*sqlx.DB, error) {
	opt := Options{}
	if len(options) > 0 {
		opt = parseGetOption(options[0])
	}
	// Check if database exist
	if opt.isTest {
		return testDbStore.getOrInit(opt)
	}
	return dbStore.getOrInit(opt)
}
