package core

import (
	"fmt"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
)

// YMLConfig config struct for .yml config file
type YMLConfig struct {
	Database DbConfig      `yaml:"database,flow"`
	Service  ServiceConfig `yaml:"service,flow"`
	Log      LogConfig     `yaml:"log,flow"`
}

// DbConfig information to connect to DB
type DbConfig struct {
	User            string `yaml:"user"`
	Database        string `yaml:"name"`
	Password        string `yaml:"password"`
	Host            string `yaml:"host"`
	Port            int    `yaml:"port"`
	DefaultTimeZone string `yaml:"tz"`
}

// ServiceConfig information on service
type ServiceConfig struct {
	Hostname     string `yaml:"host"`
	ServiceName  string `yaml:"service_name"`
	RESTPort     int    `yaml:"rest_port"`
	JWTSecretKey string `yaml:"jwt_sk"`
}

// LogConfig information on log system
type LogConfig struct {
	Level  string `yaml:"level"`
	Format string `yaml:"format"` // JSON/Console
}


func parseFromEnv() (bool, DbConfig, ServiceConfig) {
	db := DbConfig{
		User:            "tankyou_poc",
		Database:        "tankyou_poc",
		Password:        "tankyou_poc",
		Host:            "localhost",
		Port:            5432,
		DefaultTimeZone: "Europe/Paris", // Not used yet
	}
	service := ServiceConfig{
		Hostname:     "localhost",
		ServiceName:  "grpc",
		RESTPort:     3000,
		JWTSecretKey: "S0m3Secre7Token$",
	}
	log := LogConfig{
		Level: "debug",
	}
	for _, e := range os.Environ() {
		pair := strings.SplitN(e, "=", 2)
		switch pair[0] {
		// Service settings
		case "API_PORT":
			restPort, err := strconv.Atoi(pair[1])
			if err == nil {
				service.RESTPort = restPort
			}
		case "API_HOSTNAME":
			service.Hostname = pair[1]
		case "JWT_SECRET_KEY":
			service.JWTSecretKey = pair[1]
		case "SERVICE_NAME":
			service.ServiceName = pair[1]
		// Database settings
		case "DB_PORT":
			restPort, err := strconv.Atoi(pair[1])
			if err == nil {
				db.Port = restPort
			}
		case "DB_HOSTNAME":
			db.Host = pair[1]
		case "DB_NAME":
			db.Database = pair[1]
		case "DB_USER":
			db.User = pair[1]
		case "DB_PASS":
			db.Password = pair[1]
		case "DB_TIME_ZONE":
			db.DefaultTimeZone = pair[1]
		// Log config
		case "LOG_LEVEL":
			log.Level = pair[1]
		case "LOG_FORMAT":
			log.Format = pair[1]
		default:
		}
	}

	return true, db, service
}

func parseFromFile(filename string) (bool, DbConfig, ServiceConfig) {
	var config YMLConfig

	yamlFile, err := ioutil.ReadFile(filename) // nolint:gosec
	if err != nil {
		fmt.Printf("[FATAL] couldn't read config file at %s", filename)
		return false, DbConfig{}, ServiceConfig{}
	}

	err = yaml.Unmarshal(yamlFile, &config)
	if err != nil {
		fmt.Printf("[FATAL] couldn't read config file at %s", filename)
		return false, DbConfig{}, ServiceConfig{}
	}

	return true, config.Database, config.Service
}

// GetConfig retrieve configuration from provided source (default to ENV_VAR parsing)
// Return a bool to ensure config got correctly parsed, DbConfig, ServiceConfig, NewLog function
func GetConfig(filename *string) (bool, DbConfig, ServiceConfig) {
	if filename == nil {
		return parseFromEnv()
	}
	return parseFromFile(*filename)
}
