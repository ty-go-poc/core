package core

import (
	"github.com/rs/zerolog"
	"os"
	"strings"
)

var (
	// default logger to os.Stderr , level: info
	defaultLog                 = zerolog.New(os.Stderr).With().Timestamp().Logger().Level(zerolog.InfoLevel)
	log        *zerolog.Logger = nil
	levels                     = map[string]zerolog.Level{
		"debug":   zerolog.DebugLevel,
		"nolog":   zerolog.Disabled,
		"warning": zerolog.WarnLevel,
		"info":    zerolog.InfoLevel,
		"error":   zerolog.FatalLevel,
		"nolevel": zerolog.NoLevel,
		"panic":   zerolog.PanicLevel,
		"trace":   zerolog.TraceLevel,
	}
)

func stringToLevel(level string) zerolog.Level {
	lv := strings.ToLower(level)
	logLvl, ok := levels[lv]
	if !ok {
		return zerolog.InfoLevel
	}
	return logLvl
}

func newConsoleWriterLogger(level string) {
	logger := zerolog.New(zerolog.ConsoleWriter{Out: os.Stderr}).With().Timestamp().Logger().Level(stringToLevel(level))
	log = &logger
}

func newJSONLogger(level string) {
	logger := zerolog.New(os.Stderr).With().Timestamp().Logger().Level(stringToLevel(level))
	log = &logger
}

func Log(options ...LogConfig) zerolog.Logger {
	if log != nil {
		return *log
	}

	// Set new logger
	if len(options) == 0 {
		log = &defaultLog
	}

	conf := options[0]

	switch conf.Format {
	case "console", "CONSOLE":
		newConsoleWriterLogger(conf.Level)
	default:
		newJSONLogger(conf.Level)
	}

	return *log
}
