package core

import (
	"github.com/golang/protobuf/ptypes/timestamp"
	"github.com/golang/protobuf/ptypes/wrappers"
	"github.com/golang/protobuf/ptypes"
	"os"
	"time"
)

// GetEnvDefault get env variables with a default value
func GetEnvDefault(key, fallback string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}
	return fallback
}

// Int64ValueToInt64 convert Protobof int64 wrappers to a int64 native pointer
func Int64ValueToInt64(w *wrappers.Int64Value) *int64 {
	if w != nil {
		val := w.Value
		return &val
	}
	return nil
}

// Int64ToInt64Value convert int64 native pointer to a Protobof int64 wrappers
func Int64ToInt64Value(v *int64) *wrappers.Int64Value {
	if v != nil {
		val := wrappers.Int64Value{Value: *v}
		return &val
	}
	return nil
}

// Int64ValueToInt64 convert Protobof int64 wrappers to a int64 native pointer
func StringValueToString(w *wrappers.StringValue) *string {
	if w != nil {
		val := w.Value
		return &val
	}
	return nil
}

// Int64ToInt64Value convert int64 native pointer to a Protobof int64 wrappers
func StringToStringValue(v *string) *wrappers.StringValue {
	if v != nil {
		val := wrappers.StringValue{Value: *v}
		return &val
	}
	return nil
}

// Int64ValueToInt64 convert Protobof int64 wrappers to a int64 native pointer
func TimestampValueToTime(w *timestamp.Timestamp) *time.Time {
	if w != nil {
		val, err := ptypes.Timestamp(w)
		if err != nil {
			return nil
		}
		return &val
	}
	return nil
}

// Int64ToInt64Value convert int64 native pointer to a Protobof int64 wrappers
func TimeToTimeStampValue(v *time.Time) *timestamp.Timestamp {
	if v != nil {
		val, err := ptypes.TimestampProto(*v)
		if err != nil {
			return nil
		}
		return val
	}
	return nil
}
