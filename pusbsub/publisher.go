package pusbsub

import (
	"cloud.google.com/go/pubsub"
	"context"
)

const (
	DefaultProject = "sandox-259012"
)

type Publisher struct {
	Topic  *pubsub.Topic
	client *pubsub.Client
}

func NewPublisher(topicID string, projectID *string) (*Publisher, error) {
	project := DefaultProject
	if projectID != nil && *projectID != "" {
		project = *projectID
	}

	cli, err := pubsub.NewClient(context.Background(), project)
	if err != nil {
		return nil, err
	}

	topic := cli.Topic(topicID)

	publisher := Publisher{
		Topic:  topic,
		client: cli,
	}

	return &publisher, nil
}
